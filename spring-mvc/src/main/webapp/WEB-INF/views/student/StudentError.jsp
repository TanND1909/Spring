<%@ page isErrorPage="true" import="java.io.*"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Student Error</title>
</head>
<body>
	<p>
		<b>Stack trace:</b>
	</p>
	<%
		Exception exp = (Exception) request.getAttribute("javax.servlet.error.exception");
		exp.printStackTrace(new PrintWriter(out));
	%>
</body>
</html>