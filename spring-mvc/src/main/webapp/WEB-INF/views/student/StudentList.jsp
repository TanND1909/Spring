<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html>
<head>
<title>Student List</title>
<style>
#PopUp {
	display: none;
	position: absolute;
	left: 100px;
	top: 50px;
	border: 1px solid #000;
	padding: 10px;
	background-color: rgb(200, 100, 100);
	text-align: justify;
	font-size: 12px;
	width: 135px;
}
</style>
<script>
	function viewStudent(event, id) {
		var xmlHttp = new XMLHttpRequest();
		xmlHttp.open("GET", "./json/" + id, true);
		xmlHttp.onload = function() {
			if (this.status === 200) {
				var student = JSON.parse(this.responseText);
				showPos(event, '<table>' + '<tr> <td>ID:</td> <td>'
						+ student.id + '</td></tr>'
						+ '<tr> <td>Name:</td> <td>' + student.name
						+ '</td></tr>' + '<tr> <td>Age:</td> <td>'
						+ student.age + '</td></tr>' + '</table>');
			}
		};
		xmlHttp.send();
	}

	function showPos(event, text) {
		var el = document.getElementById('PopUp');
		var x, y;
		if (window.event) {
			x = window.event.clientX + document.documentElement.scrollLeft
					+ document.body.scrollLeft;
			y = window.event.clientY + document.documentElement.scrollTop
					+ document.body.scrollTop;
		} else {
			x = event.clientX + window.scrollX;
			y = event.clientY + window.scrollY;
		}
		x -= 2;
		y -= 2;
		y = y + 15;
		el.style.left = x + "px";
		el.style.top = y + "px";
		el.style.display = "block";
		document.getElementById('PopUpText').innerHTML = text;
	}
</script>
</head>
<body>
	<tiles:insertDefinition name="defaultTemplate">
		<tiles:putAttribute name="body">
			<h2>List of Students</h2>
			<table border="1">
				<tr>
					<form:form method="GET" action="list">
						<td colspan="4"><input type="text" name="q" size="30" /></td>
						<td><input type="submit" value="Submit" /></td>
					</form:form>
				</tr>
				<tr>
					<td>Id</td>
					<td>Name</td>
					<td>Age</td>
				</tr>
				<c:forEach items="${students}" var="student">
					<tr>
						<td>${student.id}</td>
						<td><a href="view/${student.id}">${student.name}</a>
						</td>
						<td>${student.getAge()}</td>
						<td><a href="edit/${student.id}">Edit</a></td>
						<td><a href="delete/${student.id}">Del</a></td>
					</tr>
				</c:forEach>
			</table>
			<div id="PopUp"
				onmouseover="document.getElementById('PopUp').style.display='none'">
				<span id='PopUpText'>TEXT</span>
			</div>
		</tiles:putAttribute>
	</tiles:insertDefinition>
</body>
</html>