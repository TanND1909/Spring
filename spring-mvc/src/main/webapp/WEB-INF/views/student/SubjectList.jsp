<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Subject List</title>
</head>
<body>

	<h2>List of Students</h2>
	<table border="1">
		<tr>
			<td>Id</td>
			<td>Title</td>
			<td>Student</td>
			<td>Score</td>
			<td colspan="2"></td>
		</tr>
		<c:forEach items="${subjects}" var="subject">
			<tr>
				<td>${subject.id}</td>
				<td>${subject.title}</td>
				<td>${subject.student.name}</td>
				<td>${subject.score}</td>
				<td><a href="edit/${subject.id}">Edit</a></td>
				<td><a href="delete/${subject.id}">Del</a></td>
			</tr>
		</c:forEach>
	</table>
	<h3>Average Score is ${average}</h3>
</body>
</html>