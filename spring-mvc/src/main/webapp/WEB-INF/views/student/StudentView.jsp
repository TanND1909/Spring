<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Student view</title>
</head>
<body>
	<h2>Student Information</h2>
	<img src="../avatar/${id}" />
	<table>
		<tr><td>Name:</td><td>${name}</td></tr>
		<tr><td>Age:</td><td>${age}</td></tr>
	</table>
	<h2>List of Subjects</h2>
	<table border="1">
		<tr>
			<td>Title</td>
			<td>Score</td>
			<c:forEach items="${subjects}" var="item">
				<tr>
					<td>${item.title}</td>
					<td>${item.score}</td>
				</tr>
			</c:forEach>
		</tr>
	</table>
</body>
</html>