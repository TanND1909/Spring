<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add New Subject Information</title>
</head>
<body>
	<h2>Please Input Subject Information</h2>
	<c:choose>
		<c:when test="${id==null}">
			<c:set var="action" value="add" />
		</c:when>
		<c:otherwise>
			<c:set var="action" value="../add" />
		</c:otherwise>
	</c:choose>
	<form:form method="POST" id="command" action="${action}">
		<form:hidden path="id" />
		<table>
			<tr>
				<td><form:label path="student">Student</form:label></td>
				<td><form:select path="student" items="${studentList}" /></td>
				<td><form:errors cssStyle="color:red" path="student" /></td>
			</tr>
			<tr>
				<td><form:label path="title">Title</form:label></td>
				<td><form:input path="title" /></td>
				<td><form:errors cssStyle="color:red" path="title" /></td>
			</tr>
			<tr>
				<td><form:label path="score">Score</form:label></td>
				<td><form:input path="score" type="number" /></td>
				<td><form:errors cssStyle="color:red" path="score" /></td>
			</tr>
			<tr>
				<td colspan="3"><input type="submit" value="submit" /></td>
			</tr>
		</table>
	</form:form>
</body>
</html>