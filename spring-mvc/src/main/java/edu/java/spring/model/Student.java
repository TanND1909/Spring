package edu.java.spring.model;

import java.util.Comparator;
import java.util.List;
import java.util.SortedSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.SortNatural;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

@XmlRootElement(name = "item")
@javax.persistence.Entity
@Table(name = "student", uniqueConstraints = { @UniqueConstraint(columnNames = "Id") })
public class Student {
	private int id;
	@NotBlank
	@Size(min = 2, max = 100)
	private String name;
	@Range(min = 1, max = 150)
	private int age;
	private SortedSet<Subject> subjects;

	public Student() {
		super();
	}

	@XmlAttribute
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@XmlElement
	@Column(name = "Name", nullable = false, length = 200)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement
	@Column(name = "age", nullable = false)
	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "student")
	@SortComparator(Student.SubjectComparator.class)
	public SortedSet<Subject> getSubjects(){
		return this.subjects;
	}
	public void setSubjects(SortedSet<Subject> subjects){
		this.subjects = subjects;
	}
	public static class SubjectComparator implements Comparator<Subject>{
		@Override
		public int compare(Subject o1, Subject o2){
			if(o1 == null || o1.getTitle() == null || o2 == null || o2.getTitle() == null)
				return 0;
			return o1.getTitle().compareToIgnoreCase(o2.getTitle());
		}
	}
}
