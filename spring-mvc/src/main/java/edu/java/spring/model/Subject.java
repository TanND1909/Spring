package edu.java.spring.model;

public class Subject implements Comparable<Subject>{
	private int id;
	
	private String title;
	
	private Student student;
	
	private int score;
	
	public void setId(int id){
		this.id = id;
	}
	
	public int getId(){
		return this.id;
	}
	
	public void setTitle(String title){
		this.title = title;
	}
	
	public String getTitle(){
		return this.title;
	}
	
	public void setStudent(Student student){
		this.student = student;
	}
	
	public Student getStudent(){
		return this.student;
	}
	
	public void setScore(int score){
		this.score = score;
	}
	
	public int getScore(){
		return this.score;
	}
	
	public int compareTo(Subject other){
		return score - other.score;
	}
	
}
