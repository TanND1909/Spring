package edu.java.spring.view;
import java.util.Map;

import javax.xml.transform.Source;

import org.springframework.web.servlet.view.xslt.XsltView;

import edu.java.spring.model.JavaClazz;
import edu.java.spring.model.StudentMapper;


public class ClazzXsltView extends XsltView{
	@Override
	protected Source locateSource(Map<String, Object> model) throws Exception {
		JavaClazz clazz = (JavaClazz) model.get("clazzObj");
		return StudentMapper.clazzToDomSource(clazz);
	}
}
