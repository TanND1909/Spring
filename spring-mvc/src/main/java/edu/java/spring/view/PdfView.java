package edu.java.spring.view;

import java.awt.Color;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.lowagie.text.Document;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import edu.java.spring.model.JavaClazz;
import edu.java.spring.model.Student;

public class PdfView extends AbstractPdfView{
	@Override
	protected void buildPdfDocument(Map<String, Object> model, Document doc,
			PdfWriter writer, HttpServletRequest request, HttpServletResponse response)
					throws Exception {
		JavaClazz clazz = (JavaClazz) model.get("clazzObj");
		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(100.0f);
		table.setWidths(new float[]{2.0f, 3.0f, 1.5f});
		table.setSpacingBefore(10);
		
		Font font = FontFactory.getFont(FontFactory.COURIER);
		font.setColor(Color.BLACK);
		
		PdfPCell cell = new PdfPCell();
		cell.setBackgroundColor(Color.CYAN);
		cell.setPadding(5);
		cell.setPhrase(new Phrase("ID", font));
		table.addCell(cell);
		
		cell.setPhrase(new Phrase("Name", font));
		table.addCell(cell);
		
		cell.setPhrase(new Phrase("Age", font));
		table.addCell(cell);
		
		table.completeRow();
		
		BaseFont times = BaseFont.createFont("times.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
		Font font1 = new Font(times, 13, Font.NORMAL);
		
		for(Student student:clazz.getStudents()){
			table.addCell(String.valueOf(student.getId()));
			PdfPCell nameCell = new PdfPCell();
			nameCell.setPhrase(new Phrase(student.getName(), font1));
			table.addCell(nameCell);
			table.addCell(String.valueOf(student.getAge()));
			table.completeRow();
		}
		
		doc.add(table);
	}
}
