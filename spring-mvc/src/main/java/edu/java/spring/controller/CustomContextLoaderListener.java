package edu.java.spring.controller;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletContextEvent;

import org.springframework.web.context.ContextLoaderListener;

public class CustomContextLoaderListener extends ContextLoaderListener {
	@Override
	public void contextDestroyed(ServletContextEvent event) {
		try {
			System.out.println("\n\n====> hibernate shutdown database\n");
			DriverManager.getConnection("jdbc:mysql://localhost:3306/springstudent;shutdown=true");
		} catch (SQLException e) {

		}
		System.out.println("\n Spring-MVC application destroyed \n");
		super.contextDestroyed(event);
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
		System.out.println("\n Spring-MVC application inited \n");
		try {
			createTables();
		} catch (Exception exp) {
			exp.printStackTrace();
		}
		super.contextInitialized(event);
	}

	private void createTables() throws Exception {
		Class.forName("com.mysql.jdbc.Driver");
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/springstudent","root","");
		try {
			createTableIfNotExist(connection, "student",
					"CREATE TABLE student(id bigint(50) unsigned NOT NULL AUTO_INCREMENT, "
							+ "NAME  varchar(1000) DEFAULT NULL, " + "age int(11) unsigned DEFAULT NULL, "
							+ "PRIMARY KEY (id)) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8");
			createTableIfNotExist(connection, "subject",
					"CREATE TABLE subject (id bigint(50) unsigned NOT NULL AUTO_INCREMENT, " + "title text,"
							+ "student bigint(50) unsigned DEFAULT NULL, " + "score int(10) unsigned DEFAULT NULL,"
							+ "PRIMARY KEY (id),KEY subject_student (student),"
							+ "CONSTRAINT subject_student FOREIGN KEY (student) REFERENCES student (id) ON DELETE CASCADE ON UPDATE CASCADE) "
							+ "ENGINE=InnoDB DEFAULT CHARSET=utf8;");
		} finally {
			connection.close();
		}
	}

	private void createTableIfNotExist(Connection connection, String tableName, String createTableSQL)
			throws SQLException {
		DatabaseMetaData dbmd = connection.getMetaData();
		ResultSet rs = dbmd.getTables(null, null, tableName.toUpperCase(), null);
		if (rs.next()) {
			System.out.println("Table " + rs.getString("TABLE_NAME") + " already exists!");
			return;
		}
		Statement statement = connection.createStatement();
		try {
			statement.execute(createTableSQL);
			System.out.println("\n\nexecuted " + createTableSQL + "\n\n");
		} finally {
			statement.close();
		}
	}
}
