package edu.java.spring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/chao")
public class HelloClazzController {
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView printMessage(){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("index");
		mav.addObject("message", "Hello Java Clazz! this is page index");
		return mav;
	}
	@RequestMapping(value="/hello",method = RequestMethod.GET)
	public ModelAndView hello(){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("index");
		mav.addObject("message", "Hello Java Clazz! this is page index test hello");
		return mav;
	}
	@RequestMapping(value="/welcome",method = RequestMethod.GET)
	public ModelAndView welcome(){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("welcome");
		mav.addObject("message", "welcome Java Clazz! this is page welcome test mvn eclipse");
		return mav;
	}
	@RequestMapping(value="/info",method = RequestMethod.GET)
	public ModelAndView info(){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("info");
		mav.addObject("message", "info Java Clazz! this is page welcome test mvn eclipse");
		return mav;
	}
}