package edu.java.spring.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.xml.sax.SAXException;

import edu.java.spring.dao.StudentHibernateDaoImpl;
import edu.java.spring.model.JavaClazz;
import edu.java.spring.model.Student;
import edu.java.spring.model.StudentMapper;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Controller
public class StudentController {
	@Autowired(required = true)
	private StudentHibernateDaoImpl studentDao;

	public StudentHibernateDaoImpl getStudentDao() {
		return studentDao;
	}

	public void setStudentDao(StudentHibernateDaoImpl studentDao) {
		this.studentDao = studentDao;
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home() {
		return "redirect:/student/list";
	}

	@RequestMapping(value = "/student/form", method = RequestMethod.GET)
	public ModelAndView student() {
		return new ModelAndView("/student/StudentForm", "command", new Student());
	}

	@RequestMapping(value = "/student/add", method = RequestMethod.POST)
	public ModelAndView addStudent(@Valid @ModelAttribute("command") Student student, BindingResult result) {
		ModelAndView model = null;
		if (result.hasErrors()) {
			model = new ModelAndView("/student/StudentForm", "command", student);
			model.addObject("errors", result);
			return model;
		}
		if (student.getId() > 0) {
			System.out.println("\n\n Edit student" + student.getId() + "\n\n");
			studentDao.update(student);
		} else {
			studentDao.insert(student);
		}
		model = new ModelAndView("redirect:/student/list");
		return model;
	}

	@RequestMapping(value = "/student/list", method = RequestMethod.GET)
	public ModelAndView listStudents(@RequestParam(value = "q", required = false) String query) {
		ModelAndView model = new ModelAndView("/student/StudentList");
		if (query == null) {
			model.addObject("students", studentDao.listStudents());
		} else {
			model.addObject("students", studentDao.loadStudent(query));
		}
		return model;
	}

	@RequestMapping(value = "/student/view/{id}")
	public String loadStudent(@PathVariable Integer id, ModelMap model) {
		Student student = new Student();

		student = studentDao.loadStudent(id);
		model.put("id", student.getId());
		model.put("name", student.getName());
		model.put("age", student.getAge());
		model.put("subjects", student.getSubjects());

		return "/student/StudentView";
	}

	@RequestMapping(value = "/student/edit/{id}", method = RequestMethod.GET)
	public ModelAndView editStudent(@PathVariable Integer id) {
		Student student = new Student();
		student = studentDao.loadStudent(id);
		ModelAndView model = new ModelAndView("/student/StudentForm", "command", student);
		model.getModelMap().put("id", student.getId());
		model.getModelMap().put("name", student.getName());
		model.getModelMap().put("age", student.getAge());
		return model;
	}

	@RequestMapping(value = "/student/delete/{id}")
	public ModelAndView delStudent(@PathVariable Integer id) {
		studentDao.delete(id);
		ModelAndView model = new ModelAndView("redirect:../list");
		model.addObject("students", studentDao.listStudents());
		return model;
	}

	@RequestMapping(value = "/student/xml", produces = MediaType.APPLICATION_XML_VALUE)
	public @ResponseBody JavaClazz viewlnXML() {
		return studentDao.getJavaClazz();
	}

	@RequestMapping(value = "/student/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody JavaClazz viewlnJSON() {
		return studentDao.getJavaClazz();
	}

	@RequestMapping(value = "/student/json/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Student viewStudent(@PathVariable Integer id) {
		return studentDao.loadStudent(id);
	}

	@RequestMapping(value = "/student/xsl")
	public ModelAndView viewXSLT() throws JAXBException, SAXException, IOException, ParserConfigurationException {
		JavaClazz clazz = studentDao.getJavaClazz();
		ModelAndView model = new ModelAndView("ClazzView");
		model.getModelMap().put("data", StudentMapper.clazzToDomSource(clazz));

		return model;
	}

	@RequestMapping(value = "/student/xsl2", produces = "application/xslt")
	public ModelAndView viewXslt2() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("clazzObj", studentDao.getJavaClazz());
		return new ModelAndView("ClazzView", map);
	}

	@RequestMapping(value = "/student/pdf", produces = "application/pdf")
	public ModelAndView viewPdf() {
		JavaClazz clazz = studentDao.getJavaClazz();
		return new ModelAndView("pdfView", "clazzObj", clazz);
	}

	@RequestMapping(value = "/student/excel", produces = "application/vnd.ms-excel")
	public ModelAndView viewExcel() {
		JavaClazz clazz = studentDao.getJavaClazz();
		return new ModelAndView("excelView", "clazzObj", clazz);
	}

	@RequestMapping(value = "/student/report", produces = "application/pdf")
	public ModelAndView viewReport() {
		List<Student> students = studentDao.listStudents();
		JRDataSource dataSource = new JRBeanCollectionDataSource(students);

		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("dataSource", dataSource);

		ModelAndView modelAndView = new ModelAndView("pdfReport", parameterMap);
		return modelAndView;
	}

	@RequestMapping(value = "/student/avatar/save", method = RequestMethod.POST)
	public String handleFormUpload(@RequestParam("id") int id, @RequestParam("file") MultipartFile file,
			HttpServletRequest request) throws IOException {
		if (file.isEmpty())
			return "/student/StudentError";

		ServletContext servletContext = request.getSession().getServletContext();
		String absoluteDiskPath = servletContext.getRealPath("/WEB-INF/views/");

		File folder = new File(absoluteDiskPath + File.separator + "avatar" + File.separator);
		if (!folder.exists())
			folder.mkdirs();

		File avatarFile = new File(folder, String.valueOf(id) + ".jpg");
		if (!avatarFile.exists())
			avatarFile.createNewFile();

		FileOutputStream outputStream = null;
		try {
			outputStream = new FileOutputStream(avatarFile);
			outputStream.write(file.getBytes());
		} finally {
			if (outputStream != null)
				outputStream.close();
		}

		return "redirect:/student/view/" + String.valueOf(id);
	}

	@RequestMapping(value = "/student/avatar/{id}", method = RequestMethod.GET)
	public ResponseEntity<byte[]> getImage(@PathVariable int id, HttpServletRequest request) throws IOException {
		ServletContext servletContext = request.getSession().getServletContext();
		String absoluteDiskPath = servletContext.getRealPath("/WEB-INF/views/");
		ByteArrayOutputStream byteOutput = new ByteArrayOutputStream();

		File folder = new File(absoluteDiskPath + File.separator + "avatar" + File.separator);
		if (folder.exists()) {
			File file = new File(folder, id + ".jpg");
			if (file.exists()) {
				FileInputStream inputStream = new FileInputStream(file);
				try {
					int read;
					byte[] bytes = new byte[4 * 1024];
					while ((read = inputStream.read(bytes)) != -1) {
						byteOutput.write(bytes);
					}
				} finally {
					if (inputStream != null)
						inputStream.close();
				}
			}
		}
		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.IMAGE_JPEG);
		return new ResponseEntity<byte[]>(byteOutput.toByteArray(), headers, HttpStatus.CREATED);
	}

}
