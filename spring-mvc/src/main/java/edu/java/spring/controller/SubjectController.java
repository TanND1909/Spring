package edu.java.spring.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import edu.java.spring.dao.StudentHibernateDaoImpl;
import edu.java.spring.dao.SubjectHibernateDaoImpl;
import edu.java.spring.model.Student;
import edu.java.spring.model.Subject;

@Controller
public class SubjectController {
	@Autowired
	private StudentHibernateDaoImpl studentDao;

	@Autowired
	private SubjectHibernateDaoImpl subjectDao;

	@RequestMapping(value = "/student/subject/form", method = RequestMethod.GET)
	public ModelAndView subjectForm() {
		ModelAndView model = new ModelAndView("/student/SubjectForm", "command", new Subject());
		List<Student> students = studentDao.listStudents();
		Map<Integer, String> map = new HashMap<Integer, String>();
		for (int i = 0; i < students.size(); i++) {
			map.put(students.get(i).getId(), students.get(i).getName());
		}
		model.getModelMap().put("studentList", map);
		return model;
	}

	@RequestMapping(value = "/student/subject/add", method = RequestMethod.POST)
	public ModelAndView addStudent(@RequestParam("student") int studentId,
			@Valid @ModelAttribute("command") Subject subject, BindingResult result) {
		Student student = studentDao.loadStudent(studentId);
		subject.setStudent(student);
		if (subject.getId() > 0) {
			subjectDao.update(subject);
		} else {
			subjectDao.insert(subject);
		}
		ModelAndView model = new ModelAndView("redirect:/student/subject/list");
		return model;
	}

	@RequestMapping(value = "/student/subject/list", method = RequestMethod.GET)
	public ModelAndView viewSubjects() {
		ModelAndView model = new ModelAndView("/student/SubjectList");
		model.addObject("subjects", subjectDao.listSubjects());
		model.addObject("average", subjectDao.averageScore());
		return model;
	}

	@RequestMapping(value = "student/subject/delete/{id}")
	public ModelAndView deleteSubject(@PathVariable Integer id) {
		subjectDao.delete(id);
		ModelAndView model = new ModelAndView("redirect:/student/subject/list");
		return model;
	}

	@RequestMapping(value = "student/subject/edit/{id}")
	public ModelAndView updateSubject(@PathVariable Integer id) {
		Subject subject = subjectDao.loadSubject(id);
		ModelAndView model = new ModelAndView("/student/SubjectForm", "command", subject);
		List<Student> students = studentDao.listStudents();
		Map<Integer, String> map = new HashMap<Integer, String>();
		for (int i = 0; i < students.size(); i++) {
			map.put(students.get(i).getId(), students.get(i).getName());
		}
		model.getModelMap().put("studentList", map);
		return model;
	}

}
