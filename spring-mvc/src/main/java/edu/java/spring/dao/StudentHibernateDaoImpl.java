package edu.java.spring.dao;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;

import edu.java.spring.model.JavaClazz;
import edu.java.spring.model.Student;

public class StudentHibernateDaoImpl implements StudentDao {
	@Autowired(required = true)
	private LocalSessionFactoryBean sessionFactory;

	public LocalSessionFactoryBean getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(LocalSessionFactoryBean sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void shutdown() {
		try {
			System.out.println("\n\n=====> hibernate shutdown\n");
			DriverManager.getConnection("jdbc:mysql:;shutdown=true");
		} catch (SQLException e) {
			Log.error(e);
		}
	}

	@Override
	public void insert(Student student) {
		Session session = sessionFactory.getObject().openSession();
		try {
			session.save(student);
			session.flush();
		} finally {
			session.close();
		}

	}

	@Override
	public JavaClazz getJavaClazz() {
		List<Student> students = listStudents();
		return new JavaClazz(students);
	}

	@Override
	public List<Student> listStudents() {
		Session session = sessionFactory.getObject().openSession();
		Query query = session.createQuery("from Student");
		try {
			return (List<Student>) query.list();
		} finally {
			session.close();
		}
	}

	@Override
	public Student loadStudent(int id) {
		Session session = sessionFactory.getObject().openSession();
		try {
			Criteria query = session.createCriteria(Student.class);
			query.add(Restrictions.eq("id", id));
			return (Student) query.list().get(0);
		} finally {
			session.close();
		}
	}

	@Override
	public void update(Student student) {
		Session session = sessionFactory.getObject().openSession();
		try {
			student = (Student) session.merge(student);
			session.update(student);
			session.flush();
		} finally {
			session.close();
		}
	}

	@Override
	public void delete(Integer id) {
		Student student = loadStudent(id);
		if (student == null)
			return;
		Session session = sessionFactory.getObject().openSession();
		try {
			session.delete(student);
			session.flush();
		} finally {
			session.close();
		}
	}

	@Override
	public List<Student> loadStudent(String name) {

		Session session = sessionFactory.getObject().openSession();
		try {
			Query query = session.createQuery("from Student where name like :studentName");
			query.setParameter("studentName", "%" + name + "%");
			return new ArrayList<Student>(query.list());
		} finally {
			session.close();
		}
	}

}
