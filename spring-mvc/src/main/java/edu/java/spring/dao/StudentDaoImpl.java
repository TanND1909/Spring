package edu.java.spring.dao;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;

import edu.java.spring.model.JavaClazz;
import edu.java.spring.model.Student;
import edu.java.spring.model.StudentMapper;

public class StudentDaoImpl implements StudentDao {
	private static Log log = LogFactory.getLog(StudentDaoImpl.class);
	private DataSource dataSource;
	private JdbcTemplate templateObject;
	private String insertSQL;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.templateObject = new JdbcTemplate(dataSource);
	}

	public JdbcTemplate getTemplateObject() {
		return templateObject;
	}

	public void setTemplateObject(JdbcTemplate templateObject) {
		this.templateObject = templateObject;
	}

	public void setInsertSQL(String insertSQL) {
		this.insertSQL = insertSQL;
	}

	public String getInsertSQL() {
		return insertSQL;
	}

	@SuppressWarnings("unused")
	private void createTableIfNotExits(String tableName, String createTableSQL) {
		try {
			DatabaseMetaData dbmd = dataSource.getConnection().getMetaData();
			ResultSet rs = dbmd.getTables(null, null, tableName.toLowerCase(), null);
			if (rs.next()) {
				log.info("Table " + rs.getString("TABLE_NAME") + "already exits !");
				return;
			}
			templateObject.execute(createTableSQL);

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void shutdown() {
		try {
			dataSource.getConnection().close();
		} catch (SQLException e) {

			log.error(e);
		}

		try {
			log.info("\n Shutdown database \n");
			DriverManager.getConnection("jdbc:mysql:;shutdown=true");
		} catch (SQLException e) {
			log.error(e);
		}
	}

	public void insert(final Student student) {
		PreparedStatementCreator creator = new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement statement = con.prepareStatement(insertSQL);
				statement.setString(1, student.getName());
				statement.setInt(2, student.getAge());
				return statement;
			}
		};

		templateObject.update(creator);
		log.info("Create record name " + student.getName());
	}

	@Autowired
	@Qualifier("studentMapper")
	StudentMapper mapper;

	public List<Student> listStudents() {
		String sql = "select * from Student";
		List<Student> students = this.templateObject.query(sql, new StudentMapper());
		return students;
	}

	public Student loadStudent(int id) {
		String SQL = "select * from Student where id = ?";
		return templateObject.queryForObject(SQL, new Object[] { id }, mapper);
	}

	public List<Student> loadStudent(String name) {
		String sql = "select * from Student where name like '%" + name + "%'";
		return templateObject.query(sql, mapper);
	}

	public void update(Student student) {
		String sql = "update Student set name =?, age=? where id= ?";
		templateObject.update(sql, student.getName(), student.getAge(), student.getId());
		log.info("Update Record with ID = " + student.getId());
		return;
	}

	public void delete(Integer id) {

		templateObject.update("delete from Student where id=?", id);
	}

	public JavaClazz getJavaClazz() {
		List<Student> students = listStudents();
		return new JavaClazz(students);
	}

}