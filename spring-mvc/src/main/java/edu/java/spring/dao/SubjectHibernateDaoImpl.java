package edu.java.spring.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.CallbackException;
import org.hibernate.Criteria;
import org.hibernate.EmptyInterceptor;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.Type;
import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;

import edu.java.spring.model.Student;
import edu.java.spring.model.Subject;

public class SubjectHibernateDaoImpl {

	//@Autowired(required = true)
	private LocalSessionFactoryBean sessionFactory;

	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(LocalSessionFactoryBean sessionFactory) {
		this.sessionFactory = sessionFactory;
		sessionFactory.getConfiguration().setInterceptor(new EmptyInterceptor() {

			@Override
			public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types)
					throws CallbackException {
				System.out.println("\n From Interception ====> Save" + entity + "\n");
				return true;
			}

			@Override
			public void onDelete(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types)
					throws CallbackException {
				System.out.println("\n From Interceptor ====> Delete" + entity + "\n");
			}

		});
	}

	public void insert(final Subject subject) {
		Session session = sessionFactory.getObject().openSession();
		try {
			session.save(subject);
			session.flush();
		} finally {
			session.close();
		}
	}

	public List<Subject> listSubjects() {

		Session session = sessionFactory.getObject().openSession();
		try {
			String sql = "select * from subject";
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity(Subject.class);
			
			List<Subject> subjects = query.list();
			if(subjects.size() < 50) 
				addBatch();
			return subjects;
		} finally {
			session.close();
		}
	}

	public Subject loadSubject(int id) {
		Session session = sessionFactory.getObject().openSession();
		try {
			Criteria query = session.createCriteria(Subject.class);
			query.add(Restrictions.eq("id", id));
			return (Subject) query.list().get(0);
		} finally {
			session.close();
		}
	}

	public void delete(Integer id) {
		Session session = sessionFactory.getObject().openSession();
		try {
			String hql = "delete from Subject where id=:subjectId";
			Query query = session.createQuery(hql);
			query.setParameter("subjectId", id);
			int result = query.executeUpdate();
			Log.info("\n\nRows affected: " + result + "\n\n");
		} finally {
			session.close();
		}
	}

	public void update(Subject subject) {
		Session session = sessionFactory.getObject().openSession();
		try {
			subject = (Subject) session.merge(subject);
			session.update(subject);
			session.flush();
		} finally {
			session.close();
		}
	}

	public int averageScore() {
		Session session = sessionFactory.getObject().openSession();
		try {
			Criteria cr = session.createCriteria(Subject.class);
			cr.setProjection(Projections.avg("score"));
			List avgScore = cr.list();
			return ((Double) avgScore.get(0)).intValue();
		} finally {
			session.close();
		}
	}
	
	private void addBatch(){
		Session session = sessionFactory.getObject().openSession();
		Transaction tx = null;
		try{
			int studentId = 1;
			tx = session.beginTransaction();
			for(int i = 0; i < 50; i++){
				Student student = (Student) session.get(Student.class, i+1);
				Subject subject = new Subject();
				subject.setTitle("Subject "+String.valueOf(i));
				subject.setStudent(student);
				subject.setScore(80);
				session.save(subject);
				studentId++;
				if(i > 0 && i % 50 == 0){
					session.flush();
					session.clear();
				}
			}
			tx.commit();
		} catch (HibernateException e){
			if(tx != null) tx.rollback();
			Log.error(e);
		} finally {
			session.close();
		}
	}

}
