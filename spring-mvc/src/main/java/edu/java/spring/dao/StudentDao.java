package edu.java.spring.dao;

import java.util.List;

import edu.java.spring.model.JavaClazz;
import edu.java.spring.model.Student;

public interface StudentDao {

	public void insert(final Student student);

	public JavaClazz getJavaClazz();

	public List<Student> listStudents();

	public Student loadStudent(int id);

	public void update(Student student);

	public void delete(Integer id);

	public List<Student> loadStudent(String name);

	public void shutdown();

}
