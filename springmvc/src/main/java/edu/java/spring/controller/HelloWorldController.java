package edu.java.spring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
 
@Controller
@RequestMapping("/")
public class HelloWorldController {
 
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView sayHello() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("index");
		mav.addObject("greeting", "Hello Java Clazz!");
		return mav;
    }
 
    @RequestMapping(value="/hello", method = RequestMethod.GET)
    public ModelAndView sayHelloAgain() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("index");
		mav.addObject("greeting", "Hello Java Clazz again!");
		return mav;
    }
 
}