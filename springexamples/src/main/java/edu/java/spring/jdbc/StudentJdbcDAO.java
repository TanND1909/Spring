package edu.java.spring.jdbc;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.StatementCallback;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

public class StudentJdbcDAO {
	private static Log log = LogFactory.getLog(StudentJdbcDAO.class);
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;
	private String insertSQL;
	private String updateAgeByNameSQL;
	private String deleteAgeByNameSQL;
	private DataSourceTransactionManager transactionManager;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplateObject = new JdbcTemplate(dataSource);
	}

	public void setInsertSQL(String insertSQL) {
		this.insertSQL = insertSQL;
	}

	public void setUpdateAgeByNameSQL(String updateAgeByNameSQL) {
		this.updateAgeByNameSQL = updateAgeByNameSQL;
	}

	public void setDeleteAgeByNameSQL(String deleteAgeByNameSQL) {
		this.deleteAgeByNameSQL = deleteAgeByNameSQL;
	}

	@SuppressWarnings("unused")
	private void createTableIfNotExits(String tableName, String createTableSQL) {
		try {
			DatabaseMetaData dbmd = dataSource.getConnection().getMetaData();
			ResultSet rs = dbmd.getTables(null, null, tableName.toLowerCase(), null);
			if (rs.next()) {
				log.info("Table " + rs.getString("TABLE_NAME") + "already exits !");
				return;
			}
			jdbcTemplateObject.execute(createTableSQL);

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void insert(String name, int age) {
		jdbcTemplateObject.update(insertSQL, name, age);
		log.info("Created Record Name=" + name + "Age=" + age);
	}

	public int totalRecord() {
		return jdbcTemplateObject.execute(new StatementCallback<Integer>() {
			public Integer doInStatement(Statement statement) {
				ResultSet rs;
				try {
					rs = statement.executeQuery("select count(*) from student");
					return new Integer(rs.next() ? rs.getInt(1) : 0);

				} catch (SQLException e) {
					e.printStackTrace();
				}
				return null;

			}
		});
	}

	public List<Student> loadStudent(String name) {
		return jdbcTemplateObject.query("select * from student where name like '" + name + "'",
				new RowMapper<Student>() {
					public Student mapRow(ResultSet rs, int i) {
						Student student = new Student();
						try {
							student.setId(rs.getInt("id"));
							student.setName(rs.getString("name"));
							student.setAge(rs.getInt("age"));
						} catch (SQLException e) {
							e.printStackTrace();
						}
						return student;
					}

				});
	}

	public void updateAgeByName(String name, int age) {
		jdbcTemplateObject.update(updateAgeByNameSQL, age, name);
		log.info("update Record Name=" + name + " Age=" + age);
	}

	public void deleteAgeByName(String name) {
		jdbcTemplateObject.update(deleteAgeByNameSQL, name);
		log.info("update Record Name=" + name);
	}

	public void setTransactionManager(DataSourceTransactionManager transactionManager) {
		this.transactionManager = transactionManager;
	}

	public int[] add(List<Student> students) {
		List<Object[]> batch = new ArrayList<Object[]>();
		for (Student student : students) {
			batch.add(new Object[] { student.getName(), student.getAge() });
		}
		System.out.println("add OK !");
		return jdbcTemplateObject.batchUpdate(insertSQL, batch);
	}

	public void save(Object name, Object age) {
		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		String countSQl = "select count(*) from student";
		try {
			int total = jdbcTemplateObject.queryForObject(countSQl, Integer.class);
			log.info("before delete data, total record is " + total);
			String sql = "delete from student where name like '" + name + "'";
			jdbcTemplateObject.execute(sql);
			total = jdbcTemplateObject.queryForObject(countSQl, Integer.class);

			log.info("before save data, total record is " + total);
			sql = "insert into Student (name,age) values (?,?)";
			jdbcTemplateObject.update(sql, name, age);

			transactionManager.commit(status);
		} catch (Exception exp) {
			exp.printStackTrace();
			transactionManager.rollback(status);
			int total = jdbcTemplateObject.queryForObject(countSQl, Integer.class);

			log.info("after rollback, total record is " + total);
		}

	}
}
