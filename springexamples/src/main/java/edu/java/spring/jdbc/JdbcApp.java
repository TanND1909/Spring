package edu.java.spring.jdbc;

import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class JdbcApp {
	public static void main(String[] args) {

		System.out.println("Staring connect to mysql DB...");
		@SuppressWarnings("resource")
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
		StudentJdbcDAO jdbc = (StudentJdbcDAO) context.getBean("studentJdbcDAO");
		System.out.println("Connected to mysql DB!");
		
/*		// jdbc.insert("Tran Van A", 24);
		// System.out.println("Insert done!");
		System.out.println("Total student is " + jdbc.totalRecord());

		List<Student> students = jdbc.loadStudent("Tran Van A");
		for (Student student : students) {
			System.out.println(student);
		}
		System.out.println("load done!");

		jdbc.updateAgeByName("Tran Van A", 25);
		System.out.println("Update done!");

		jdbc.deleteAgeByName("Tran Van A");
		System.out.println("delete done!");

		students.add(new Student("Tran Thi A", 17));
		students.add(new Student("Le Van L", 20));
		students.add(new Student("Phan Thi Z", 25));

		int[] insertCounts = jdbc.add(students);

		System.out.println("Total student in list is " + students.size());

		for (int i = 0; i < insertCounts.length; i++) {
			System.out.println("add record " + i + " : " + (insertCounts[i] == 0 ? "failed" : "success"));
		}*/
		
		jdbc.save("Nguyen Duy Tan", "23");
		System.out.println("Total student is " + jdbc.totalRecord());
	}
}
