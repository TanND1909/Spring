package edu.java.spring;

import java.util.List;

import org.springframework.beans.factory.DisposableBean;

public class HelloClazz implements DisposableBean {
	private String msg;
	private List<JavaClazz> clazzes;
	
	public HelloClazz() {
		super();
		this.msg = "From Contructor: Say hello to everyone!";
	}
	public HelloClazz(int person) {
		super();
		this.msg = "From Contructor: Say to "+ person + " person(s)!";
	}
	public HelloClazz(HelloClazz clazz) {
		super();
		this.msg = clazz.msg;
	}
	
	public void setMsg(String msg){
		this.msg = "Call Form Setter: " + msg;
	}
	public String getMsg(){
		return msg;
	}
	public void printMsg(){
		System.out.println("Your msg : "+ msg);
	}
	private void initMsg(){
		System.out.println("Calling init method");
		this.msg = "From init method : say hello bean!";
	}
	
	public void destroy() throws Exception {
		this.msg = null;
		System.out.println("msg is null");
	}
	public List<JavaClazz> getClazzes() {
		return clazzes;
	}
	public void setClazzes(List<JavaClazz> clazzes) {
		this.clazzes = clazzes;
	}
	
	
}
