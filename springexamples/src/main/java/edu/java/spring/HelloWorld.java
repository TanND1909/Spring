package edu.java.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;

public class HelloWorld {
	private String msg;
	
	@Autowired(required=true)
	@Qualifier("helloJavaClazz2")
	private HelloClazz clazz;
	
	public HelloWorld() {

	}
	public HelloWorld(String name, HelloClazz clazz) {
		super();
		this.msg = "From HelloWorld contructor: " + name + " : " + clazz.getMsg();
	}
	@Required
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public void setClazz(HelloClazz clazz) {
		this.clazz = clazz;
	}

	public void sayHello(){
		clazz.printMsg();
		System.out.println("From HelloWorld: " + msg );
	}
}
