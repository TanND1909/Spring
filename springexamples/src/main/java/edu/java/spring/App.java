package edu.java.spring;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		@SuppressWarnings("resource")
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
		context.start();
		context.registerShutdownHook();

		System.out.println("------------------------------------------------------------------");
		HelloWorld hello = (HelloWorld) context.getBean("helloWorld");
		hello.sayHello();
		context.stop();

	}
}
