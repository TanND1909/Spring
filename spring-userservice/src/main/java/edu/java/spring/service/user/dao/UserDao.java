package edu.java.spring.service.user.dao;

import java.util.List;

import edu.java.spring.service.user.model.User;

public interface UserDao {
	void save(User user);
	
	void update(User user);
	
	List<User> getUsers();
	
	void delete(User user);
	
	User getUser(String username);
}
