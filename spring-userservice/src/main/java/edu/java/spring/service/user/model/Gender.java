package edu.java.spring.service.user.model;

public enum Gender {
	MALE("Male"), FEMALE("Female"), OTHER("Other");
	
	private String value;
	
	Gender(String value){
		this.value = value;
	}
	
	public String getGender(){
		return value;
	}
}
