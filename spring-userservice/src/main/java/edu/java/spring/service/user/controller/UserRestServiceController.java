package edu.java.spring.service.user.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.xml.MarshallingView;

import edu.java.spring.service.user.dao.UserDaoImpl;
import edu.java.spring.service.user.model.Gender;
import edu.java.spring.service.user.model.User;

@Controller
public class UserRestServiceController {

	@Autowired
	private UserDaoImpl userDao;

	@Autowired
	private View jsonView;

	@Autowired
	private MarshallingView xmlView;

	@RequestMapping(value = "/rest/user/{username}")
	public ModelAndView getUser(@PathVariable("username") String name) {
		return new ModelAndView(jsonView, "data", userDao.getUser(name));
	}

	@RequestMapping(value = { "/rest/user/" })
	public ModelAndView getUsers() {
		return new ModelAndView(jsonView, "data", userDao.getUsers());
	}

	@RequestMapping(value = "/rest/del/{username}")
	public ModelAndView deleteUser(@PathVariable("username") String name) {
		User user = userDao.getUser(name);
		userDao.delete(user);
		return new ModelAndView(jsonView, "data", user);
	}

	@RequestMapping(value = "/rest/form", method = RequestMethod.GET)
	public ModelAndView userForm() {
		ModelAndView model = new ModelAndView("UserForm", "command", new User());
		Map<Gender, String> genders = new HashMap<Gender, String>();
		for (Gender gender : Gender.values()) {
			genders.put(gender, gender.getGender());
		}
		Map<Integer, String> marrieds = new HashMap<Integer, String>();
		marrieds.put(0, "No");
		marrieds.put(1, "Yes");
		model.getModelMap().put("genders", genders);
		model.getModelMap().put("marrieds", marrieds);
		return model;
	}

	@RequestMapping(value = "/rest/add", method = RequestMethod.POST)
	public ModelAndView addUser(@Valid @ModelAttribute("command") User user, BindingResult result) {
		// Check data
		if (result.hasErrors()) {
			ModelAndView model = new ModelAndView("UserForm", "command", user);
			// Setup view
			Map<Gender, String> genders = new HashMap<Gender, String>();
			for (Gender gender : Gender.values()) {
				genders.put(gender, gender.getGender());
			}
			Map<Integer, String> marrieds = new HashMap<Integer, String>();
			marrieds.put(0, "No");
			marrieds.put(1, "Yes");
			model.getModelMap().put("genders", genders);
			model.getModelMap().put("marrieds", marrieds);
			// Add error messages
			model.addObject("errors", result);
			return model;
		}
		userDao.save(user);
		return new ModelAndView(jsonView, "data", user);
	}

	@RequestMapping(value ="/rest/user/create", method =RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public ModelAndView createUser(@RequestBody User user) {
		try {
			userDao.save(user);
		} catch (Exception e) {
			return new ModelAndView(jsonView, "error", e.toString());
		}
		return new ModelAndView(jsonView, "data", user);
	}

	@RequestMapping(value ="/rest/user/update", method = RequestMethod.POST )
	@ResponseStatus(HttpStatus.CREATED)
	public ModelAndView updateUser(@RequestBody User user) {
		try {
			userDao.update(user);
		} catch (Exception e) {
			return new ModelAndView(jsonView, "error", e.toString());
		}
		return new ModelAndView(jsonView, "data", user);
	}

	@RequestMapping(value = "/rest/user/xml/{username}", produces = MediaType.APPLICATION_XML_VALUE)
	public ModelAndView getUserAsXML(@PathVariable("username") String name) {
		return new ModelAndView(xmlView, "data", userDao.getUser(name));
	}
}
