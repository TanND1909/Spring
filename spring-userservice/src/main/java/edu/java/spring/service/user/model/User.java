package edu.java.spring.service.user.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="user", uniqueConstraints={@UniqueConstraint(columnNames = "username")})
@XmlRootElement(name="user")
public class User {

	@NotBlank
	@Size(min=2, max=20)
	private String username;

	@NotBlank
	@Size(min=2, max=20)
	private String password;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date birthday;

	private int age;
	
	private Gender gender;
	
	@Email
	private String email;
	
	private boolean married;
	
	public User(){

	}

	public User(String username, String password){
		this.username = username;
		this.password = password;
	}

	@Id
	@Column(name="username", unique=true, nullable=false)
	@XmlElement
	public String getUsername(){
		return username;
	}

	public void setUsername(String username){
		this.username = username;
	}

	@Column(name="password", nullable=false)
	@XmlElement
	public String getPassword(){
		return password;
	}

	public void setPassword(String password){
		this.password = password;
	}

	@Column(name="gender", nullable=false)
	public Gender getGender(){
		return gender;
	}

	public void setGender(Gender gender){
		this.gender = gender;
	}

	@Column(name="age", nullable=true)
	@XmlAttribute
	public int getAge(){
		return age;
	}

	public void setAge(int age){
		this.age = age;
	}

	@JsonSerialize(using=DateSerializer.class)
	@JsonDeserialize(using=DateDeserializer.class)
	@Column(name="birthday", nullable=false)
	public Date getBirthday(){
		return birthday;
	}

	public void setBirthday(Date birthday){
		this.birthday = birthday;
	}
	
	@Column(name="email", nullable=false)
	public String getEmail(){
		return email;
	}
	
	public void setEmail(String email){
		this.email = email;
	}
	
	@Column(name="married", nullable=false)
	public boolean getMarried(){
		return married;
	}
	
	public void setMarried(boolean married){
		this.married = married;
	}
}
