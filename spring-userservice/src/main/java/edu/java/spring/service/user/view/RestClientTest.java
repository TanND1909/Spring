package edu.java.spring.service.user.view;

import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import edu.java.spring.service.user.model.Gender;
import edu.java.spring.service.user.model.User;
import sun.misc.BASE64Encoder;

public class RestClientTest {
	private static String getUser(String username) throws Exception {
		URL url = new URL("http://localhost:8080/spring-userservice/rest/user/" + username);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		//BASE64Encoder encoder = new BASE64Encoder();
		//String encoded = encoder.encode("hanoijava:hanoijava".getBytes());
		//connection.setRequestProperty("Authorization", "Basic " + encoded);
		connection.setRequestProperty("Accept", "application/json");
		return readInputStream(connection.getInputStream());
	}

	private static String readInputStream(InputStream stream) throws Exception {
		StringWriter writer = new StringWriter();
		try {
			int read;
			byte[] bytes = new byte[4 * 1024];
			while ((read = stream.read(bytes)) != -1) {
				writer.write(new String(bytes, 0, read));
			}
		} finally {
			stream.close();
			writer.close();
		}
		return writer.getBuffer().toString();
	}

	private static String getUserAsXML(String username) throws Exception {
		URL url = new URL("http://localhost:8080/spring-userservice/rest/user/" + username);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		//BASE64Encoder encoder = new BASE64Encoder();
		//String encoded = encoder.encode("hanoijava:hanoijava".getBytes());
		//connection.setRequestProperty("Authorization", "Basic " + encoded);
		return readInputStream(connection.getInputStream());
	}

	private static String createUser(User user) throws Exception {
		return postUser(new URL("http://localhost:8080/spring-userservice/rest/user/create"), user);
	}

	private static String updateUser(User user) throws Exception {
		return postUser(new URL("http://localhost:8080/spring-userservice/rest/user/update"), user);
	}

	private static String deleteUser(String username) throws Exception {
		URL url = new URL("http://localhost:8080/spring-userservice/rest/del/" + username);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.setRequestProperty("Accept", "application/json");
		return readInputStream(connection.getInputStream());
	}

	private static String postUser(URL url, User user) throws Exception {
		URLConnection connection = url.openConnection();
		HttpURLConnection httpConnection = (HttpURLConnection) connection;
		httpConnection.setRequestMethod("POST");
		connection.addRequestProperty("Content-Type", "application/json");
		connection.addRequestProperty("Accept", "application/json");
		connection.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
		ObjectMapper mapper = new ObjectMapper();
		StringWriter writer = new StringWriter();
		mapper.writeValue(writer, user);
		wr.writeBytes(writer.getBuffer().toString());
		wr.flush();
		wr.close();
		return readInputStream(connection.getInputStream());
	}

	private static String getUsers() throws Exception {
		URL url = new URL("http://localhost:8080/spring-userservice/rest/user/");
		URLConnection connection = url.openConnection();
		HttpURLConnection httpConnection = (HttpURLConnection) connection;
		httpConnection.setRequestMethod("GET");
		connection.addRequestProperty("Accept", "application/json");
		return readInputStream(connection.getInputStream());
	}

	public static void main(String[] args) throws Exception {

//		User user = new User("test1", "123456");
//		user.setAge(45);
//		Calendar calendar = Calendar.getInstance();
//		calendar.set(Calendar.YEAR, 1960);
//		user.setBirthday(calendar.getTime());
//		user.setEmail("test1@gmail.com");
//		user.setGender(Gender.MALE);
//		user.setMarried(false);
		//createUser(user);
		//System.out.println("Rest Response:"+getUser("test1"));
		//System.out.println(getUsers());

		/* ========================== */
		// String json = getUsers();
		// ObjectMapper mapper = new ObjectMapper();
		// JsonNode jsonNode = mapper.readValue(json, JsonNode.class);
		// JsonNode userCards = jsonNode.path("data");
		// List<RestUser> users = mapper.readValue(userCards.toString(), new
		// TypeReference<List<RestUser>>(){});
		// for(int i = 0; i < users.size(); i++){
		// System.out.println("user "+i+" is "+users.get(i).getUsername());
		// }

		/* ========================== */
		//System.out.println(getUser("demo"));
		//System.out.println(getUserAsXML("demo"));
		String json = getUser("demo");
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readValue(json, JsonNode.class);
		JsonNode userCards = jsonNode.path("data");
		User user = mapper.readValue(userCards.toString(),User.class);
		user.setEmail("demo@miyatsu.vn");
		updateUser(user);
		System.out.println(getUser("demo"));

		/* ========================== */
		//System.out.println(deleteUser("del"));

		/* ========================== */
		// System.out.println("Rest Response: "+getUserAsXML("viet3695"));
	}
}
