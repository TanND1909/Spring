package edu.java.spring.service.user.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;

import edu.java.spring.service.user.dao.UserDao;
import edu.java.spring.service.user.model.User;

public class UserDaoImpl implements UserDao{

	@Autowired
	private LocalSessionFactoryBean sessionFactory;
	
	@Override
	public void save(User user) {
		Session session = sessionFactory.getObject().openSession();
		try{
			session.save(user);
			session.flush();
		} finally{
			session.close();
		}
	}

	@Override
	public void update(User user) {
		Session session = sessionFactory.getObject().openSession();
		try{
			user = (User) session.merge(user);
			session.update(user);
			session.flush();
		} finally {
			session.close();
		}
	}

	@Override
	public List<User> getUsers() {
		Session session = sessionFactory.getObject().openSession();
		Query query = session.createQuery("from User");
		try{
			return (List<User>) query.list();
		} finally {
			session.close();
		}
	}

	@Override
	public void delete(User user) {
		Session session = sessionFactory.getObject().openSession();
		try{
			session.delete(user);
			session.flush();
		} finally{
			session.close();
		}
	}

	@Override
	public User getUser(String username) {
		Session session = sessionFactory.getObject().openSession();
		try{
			Criteria query = session.createCriteria(User.class);
			query.add(Restrictions.eq("username", username));
			return (User) query.list().get(0);
		} finally {
			session.close();
		}
	}

}
