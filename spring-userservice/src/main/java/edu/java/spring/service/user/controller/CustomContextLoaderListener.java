package edu.java.spring.service.user.controller;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletContextEvent;

import org.springframework.web.context.ContextLoaderListener;

public class CustomContextLoaderListener extends ContextLoaderListener {

	@Override
	public void contextInitialized(ServletContextEvent event) {
		System.out.println("\n\n Spring-Service application inited \n\n");
		try {
			createTables();
		} catch (Exception exp) {
			exp.printStackTrace();
		}
		super.contextInitialized(event);
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		try {
			System.out.println("\n\n====> hibernate shutdown database\n");
			DriverManager.getConnection("jdbc:mysql://localhost:3306/springservice;shutdown=true");
		} catch (SQLException e) {

		}
		System.out.println("\n Spring-Service application destroyed \n\n");
		super.contextDestroyed(event);
	}

	private void createTables() throws Exception {
		Class.forName("com.mysql.jdbc.Driver");
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/springservice", "root", "");
		try {
			createTableIfNotExist(connection, "user", "CREATE TABLE user (username varchar(255) NOT NULL, "
					+ "password varchar(255) DEFAULT NULL, " + "gender varchar(255) DEFAULT NULL, "
					+ "age int(10) unsigned DEFAULT NULL, "
					+ "birthday date DEFAULT NULL, email varchar(255) DEFAULT NULL, "
					+ "married tinyint(1) unsigned DEFAULT '0',PRIMARY KEY (username)) ENGINE=InnoDB DEFAULT CHARSET=utf8");
		} finally {
			connection.close();
		}
	}

	private void createTableIfNotExist(Connection connection, String tableName, String createTableSQL)
			throws SQLException {
		DatabaseMetaData dbmd = connection.getMetaData();
		ResultSet rs = dbmd.getTables(null, null, tableName.toUpperCase(), null);
		if (rs.next()) {
			System.out.println("Table " + rs.getString("TABLE_NAME") + " already exists!");
			return;
		}
		Statement statement = connection.createStatement();
		try {
			statement.execute(createTableSQL);
			System.out.println("\n\nexecuted " + createTableSQL + "\n\n");
		} finally {
			statement.close();
		}
	}
}
