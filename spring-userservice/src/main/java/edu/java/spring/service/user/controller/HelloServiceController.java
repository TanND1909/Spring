package edu.java.spring.service.user.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloServiceController {
	@RequestMapping(value="/{name}", method=RequestMethod.GET)
	public @ResponseBody String sayHello(@PathVariable String name){
		String result = "Java Clazz say hello to "+name;
		return result;
	}
	
}
