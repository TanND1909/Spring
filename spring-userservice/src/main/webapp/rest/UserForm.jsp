<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>User Form</title>
</head>
<body>
	<form:form id="command" method="POST" action="add">
		<table>
			<tr>
				<td><form:label path="username">Username</form:label></td>
				<td><form:input path="username" /></td>
				<td><form:errors cssStyle="color:red" path="username" /></td>
			</tr>
			<tr>
				<td><form:label path="password">Password</form:label></td>
				<td><form:password path="password" /></td>
				<td><form:errors cssStyle="color:red" path="password" /></td>
			</tr>
			<tr>
				<td><form:label path="gender">Gender</form:label></td>
				<td><form:select path="gender" items="${genders}" /></td>
				<td><form:errors cssStyle="color:red" path="gender" /></td>
			</tr>
			<tr>
				<td><form:label path="birthday">Birthday</form:label></td>
				<td><form:input path="birthday" /></td>
				<td><form:errors cssStyle="color:red" path="birthday" /></td>
			</tr>
			<tr>
				<td><form:label path="age">Age</form:label></td>
				<td><form:input path="age" /></td>
				<td><form:errors cssStyle="color:red" path="age" /></td>
			</tr>
			<tr>
				<td><form:label path="email">Email</form:label></td>
				<td><form:input path="email" /></td>
				<td><form:errors cssStyle="color:red" path="email" /></td>
			</tr>
			<tr>
				<td><form:label path="married">Married</form:label></td>
				<td><form:radiobuttons path="married" items="${marrieds}"/></td>
				<td><form:errors cssStyle="color:red" path="married" /></td>
			</tr>
			<tr>
				<td colspan="3"><input type="submit" value="submit" id="submit"/></td>
			</tr>
		</table>
	</form:form>
</body>
</html>